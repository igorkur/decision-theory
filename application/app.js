$(function () {

  var tab = "#poln";

  handleTab(tab);

  $('#menu a').click(function (e) {
    e.preventDefault();
    tab = e.currentTarget.hash;
    $(this).tab('show');
    handleTab(tab);
  });


  function addInput(container, value) {
    console.log(value)
    var appended = "<div class='input-group'> \
      <span class='input-group-addon'> \
        <input type='checkbox' id='checkbox'> \
      </span> \
      <input name='input' type='text' id='input' class='form-control' value='" + value + "'/> \
    </div>";
    $(appended).appendTo($(container));
  }

  function removeInput(container) {
    $(container).remove();
  }

  function handleTab(tab) {

    $(tab + ' #openFile').click(function () {
      $(tab + " #filename").trigger('click');
    });

    $(tab + ' #addField').click(function(){
      addInput(tab + ' .controls .inputs', '');
    });

    $(tab + ' #removeField').click(function(){
      removeInput(tab + ' .input-group:last');
    });

    handleFile(tab, tab + ' #filename');

    $(tab + ' #submit').click(function() {
      var inputs = $(tab + ' .controls #input'),
          min = $(tab + ' .minmax #min'),
          max = $(tab + ' .minmax #max'),
          min_izb,
          max_izb,
          poln;

      handleInputs(inputs, min, max, function(result){
        console.log(result);
        max_izb = (result.total - result.max) / (result.max - result.min);
        min_izb =  (result.min - 0) / (result.max - result.min);
        poln = (result.total - result.min) / (result.max - result.min);

        $(tab + ' #max_izb').val(max_izb);
        $(tab + ' #min_izb').val(min_izb);

        $(tab + ' #poln_val').val(poln);

        $(tab + ' #calculations').show();

        $(tab + ' #container').highcharts({
           xAxis: {
             categories: ['']
           },
           yAxis: {
             title: {
                text: 'Уровень'
              },
             min:0
           },
           legend: {
            enabled:false
           }
         });

        drawYAxis(tab + ' #container', 0, 'red', "П\u207B\u2098="+0);

        for (var item in result) {
          switch (item) {
            case 'total':
              drawYAxis(tab + ' #container', result[item], 'red', "П\u207A\u2098="+result[item]);
              break;
            case 'min':
              drawYAxis(tab + ' #container', result[item], 'yellow', "П\u207B="+result[item]);
              break;
            case 'max':
              drawYAxis(tab + ' #container', result[item], 'yellow', "П\u207A="+result[item]);
              break;
            case 'current':
              drawYAxis(tab + ' #container', result[item], 'cyan', "П\u209B="+result[item]);
              break;
            default:
              console.log('ERRORS');
          }
        }
      });
    });
  }

  function handleFile(tab, input) {

    $(input).change(function(e) {

      if (e.target.files !== undefined) {
        removeInput(tab + ' .input-group:last');
        var reader = new FileReader();
        reader.onload = function(e) {
          var lines = e.target.result.split("\n");

          for(var i = 0; i < lines.length - 1; i++) {

            addInput(tab + ' .controls .inputs', lines[i].toString());
          }
        };
        reader.readAsText(e.target.files.item(0));
      }
    });
  }

  function handleInputs(inputs, min, max, callback) {
    var total = 0,
        current = 0,
        result = {},
        condition = true;

    inputs.each(function() {
      var value = $(this).val();
      total++;
      if(value !== '' && min.val() !== '' && max.val() !== '') {
        if ($(this).prev().children().is(':checked')) {
          current++;
        }
        result.min = min.val();
        result.max = max.val();
        $(this).parent().removeClass('has-error');
      }
      else {
        condition &= false;
        if (min.val() === '') {
          min.parent().addClass('has-error');
        }
        else{
          min.removeClass('has-error');
        }
        if (max.val() === '') {
          max.parent().addClass('has-error');
        }
        else{
          max.parent().removeClass('has-error');
        }
        $(this).parent().addClass('has-error');
      }
    });

    result.total = total;
    result.current = current;

    if (condition) {
      callback(result);
      $('.alert').hide();
    }
    else {
      $('.alert').append("All fields must be filled!").show();
    }
  }

  function drawYAxis(container, value, color, label) {
    var chart = $(container).highcharts();
    chart.addSeries({
      type: 'scatter',
      marker: {
        enabled: false
      },
      data: [Number(value)]
    });
    chart.yAxis[0].addPlotLine({
        value : Number(value),
        color : color,
        width : 3,
        label : {
          text: label
        }
    });
  }

});
